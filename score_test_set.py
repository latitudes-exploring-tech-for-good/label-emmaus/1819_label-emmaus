# Script to compute the algorithm score

import clustering.algorithms
import pre_processing.cleaning
import pre_processing.volume_number_detection
import scoring.scoring
import pandas as pd
import datetime as dt
import numpy as np


print("loading dataset ...")
data = pd.read_csv("data/test_set.csv")
data.rename({'Unnamed: 0': 'id', 'AUTHOR': 'author', 'TITLE': 'title'}, axis=1, inplace=True)
# data.set_index(['id'], inplace=True)
# data['id_cluster'] = 0
data = data.loc[:5000]
print(list(data))


print("pre-processing ...")
data.author = pre_processing.cleaning.clean_df_column(data.author)
data.title = pre_processing.cleaning.clean_df_column(data.title)
data = pre_processing.volume_number_detection.add_volume_column_and_replace_title(data)

print("clustering ...")
start = dt.datetime.now()
dummy_names = clustering.algorithms.frequent_authors(data, 0.25)
author_dict, cluster_dict, output_df, access_authors, levenstein = clustering.algorithms.clustering_v_3(data, 85, dummy_names)
stop = dt.datetime.now()
delta_t = stop - start
print("clustering time :", delta_t)

print("mean access time", np.array(access_authors).mean())

print("mean levenstein time", np.array(levenstein).mean())

#data.reset_index(inplace=True)
#output_df.reset_index(inplace=True)

print("computing score ...")
precision, recall, F_score = scoring.scoring.performance(output_df, data)
output_df.to_csv('data_clustered_tomes')

print("precision :", precision)
print("recall :   ", recall)
print("F_score :  ", F_score)

print("finished !")

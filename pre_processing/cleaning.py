import re


def clean_df_column(df_column):
    """Applies the cleaning function "clean_string" to a column of a data frame whose data type is "str"."""
    if type(df_column) == str:
        return clean_string(df_column)
    else:
        return df_column.apply(lambda x: clean_string(x))


def remove_words_from_df_columns(df_column, to_remove):
    """Removes the words in 'to_remove' list from a df column by calling the function "remove_words_from_field"."""
    return df_column.apply(lambda x: remove_words_from_field(x, to_remove))


def clean_string(string):
    """Cleans a string by suppressing any capital letters, special characters, double spaces. Returns the new string."""
    if not type(string) is str:
        raise TypeError("Expected type str")

    # Remove special chars
    str_punctuation = re.sub('\W+', ' ', string)
    # Lower case
    str_lowercase = str_punctuation.lower()
    # Remove double spaces
    str_double_spaces = re.sub('\s+', ' ', str_lowercase).strip()
    # Translates special chars
    dict1 = str.maketrans('0123456789abcdefghijklmnopqrstuvwxyzªµºãàáâåäçèéêëìíîïñõòóôöøùúûüÿƒπω',
                          '0123456789abcdefghijklmnopqrstuvwxyzauoaaaaaaceeeeiiiinoooooouuuuyfpw')
    str_special_chars = str_double_spaces.translate(dict1)
    str_out = str_special_chars.replace("œ", "oe").replace("ﬁ", "fi").replace("ﬂ", "fl").replace("æ", "ae").replace("ß", "ss")
    return str_out


def remove_words_from_field(field, to_remove):
    """ Removes the words in 'to_remove' list from a field and returns the processed field."""
    if not type(field) is str:
        raise TypeError("Expected type str")

    field = field.split()
    for word in to_remove:
        try:
            field.remove(word)
        except ValueError:
            pass
    field = ' '.join(field)
    return field


def sort_string(x):
    """
    Sorts words in a string, and returns the sorted string.
    :param x: string
    :return: string of sorted words
    """
    a = x.split()
    a.sort()
    a = ' '.join(a)
    return a


def remove_dummy_names_from_author(dummy_names, authors):
    """ Removes the names in 'dummy_names' from the field 'author' while keeping at least one word in the field, then
    returns the processed field. It is used to remove the most common names in the field 'author'. """
    authors = str(authors).split()
    for name in dummy_names:
        if len(authors) <= 1:
            break
        else:
            if name in authors:
                authors.remove(name)
    return authors

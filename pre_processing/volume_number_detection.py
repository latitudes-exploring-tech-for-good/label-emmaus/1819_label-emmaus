import re
from pre_processing import cleaning


def add_volume_column_and_replace_title(df):
    """
    This function is called during the pre-processing phase of the table containing books to cluster. It removes the
    volume information contained in the column 'title' and writes it into a new column named 'volume', and it returns
    the modified table.
    """
    df['title'] = cleaning.clean_df_column(df['title'])
    results = df['title'].apply(extract_volume_info)
    df['VOLUME'] = [results[i][0] for i in df.index]
    df['title'] = [results[i][1] for i in df.index]
    return df


def extract_volume_info(title):
    """
    Function that identifies if a title contains a volume information. It returns the volume number where appropriate,
    and the title cleaned from the volume information. In case of multi-volume books, it returns the code '-2', and if
    no volume information is found, it returns '-1'

    In practice, it uses regular expression to identify each pattern which could be a volume information.

    :param title: a string
    :return: a tuple composed of :
                - the volume number if found, '-2' in case of multi-volumes, '-1' if no volume information is found
                - the title, cleaned from the volume information if found, or without modifications
    """
    # Initialization of variables
    keywords_list = [' tome ', ' t ', ' volume ', ' vol ', ' t', ' livre ']
    multivol_keywords_list = [' tomes ', ' tomes$', ' volumes ', ' volumes$', ' lot ', '^lot ']
    volume = None
    multi_volumes = False
    # A. (multi vol) Check if the title contains some keywords specific to multi volumes book, and remove these keywords
    for keyword in multivol_keywords_list:
        if re.findall('(?i)'+keyword, title):
            multi_volumes = True
            if '$' in keyword:
                keyword = keyword.replace('$', '')
            elif '^' in keyword:
                keyword = keyword.replace('^', '')
            title = title.replace(keyword, ' ')
    else:
        # B. (vol) Check if the title contains some keywords specific to volume information
        for keyword in keywords_list:
            if volume:
                break
            # C. (multi vol) Check the keyword occurrence : if many, it is a multi volumes book. Remove these keywords.
            elif len(re.findall('(?i)'+keyword, title)) > 1 and keyword != ' t' and keyword != ' t ':
                multi_volumes = True
                title = title.replace(keyword, ' ')  # Remove the keywords in the title
            # D. (vol) In case of single occurrence, look for the volume number in the word following the keyword
            elif len(re.findall('(?i)'+keyword, title)) == 1:
                split_title = re.split('(?i)'+keyword, title)
                separate_words = re.split(' ', split_title[1])
                # Try the 3 ways to write a number (arabic, roman and literal) and keep the one giving a result
                arabic_nbr = extract_arabic_number(separate_words[0])
                roman_nbr = extract_roman_number(separate_words[0])
                literal_nbr = extract_literal_number(separate_words[0])
                if arabic_nbr:
                    volume = arabic_nbr
                elif roman_nbr and roman_nbr != 'd' and roman_nbr != 'cd':
                    volume = decode_roman_number(roman_nbr)
                elif literal_nbr:
                    volume = literal_nbr
                # E. Final treatments when a volume number is found:
                if volume:
                    # Check if the volume number is followed by 'et', which means it is a multi volumes book.
                    # Then clear the volume information in the title: remove volume info and join the title elements
                    if len(separate_words) > 2 and re.match('(?i)^et$', separate_words[1]):
                        multi_volumes = True
                        title = ' '.join([split_title[0], ' '.join(separate_words[3:len(separate_words)])])
                    else:
                        separate_words.pop(0)
                        title = ' '.join([split_title[0], ' '.join(separate_words)])
    # Returns either the volume number or a specific code, and the cleaned title
    if multi_volumes:
        return -2, title
    elif volume:
        return volume, title
    else:
        return -1, title


def extract_arabic_number(string):
    """Identifies if a string is an arabic numeral, and returns this arabic numeral where appropriate, else None."""
    if re.findall('[0-9]+', string):
        return re.findall('[0-9]+', string)[0]
    else:
        return None


def extract_roman_number(string):
    """Identifies if a string is a roman numeral, and returns this roman numeral where appropriate, else None."""
    if re.findall('(?i)^(M{0,4})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$', string):
        return ''.join(re.findall('(?i)^(M{0,4})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$', string)[0])
    else:
        return None


def decode_roman_number(roman):
    """Converts a roman numeral, written in lowercase, into an arabic number"""
    trans = {'i': 1, 'v': 5, 'x': 10, 'l': 50, 'c': 100, 'd': 500, 'm': 1000}
    values = [trans[r] for r in roman]
    return sum(
        val if val >= next_val else -val
        for val, next_val in zip(values[:-1], values[1:])
    ) + values[-1]


def extract_literal_number(string):
    """Identifies if a string is a literal numeral, and returns the arabic numeral corresponding, else None."""
    trans = {'premier': 1, 'un': 1, 'second': 2, 'deuxieme': 2, 'deux': 2, 'troisieme': 3, 'trois': 3, 'quatrieme': 4,
             'quatre': 4, 'cinquieme': 5, 'cinq': 5, 'sixieme': 6, 'six': 6, 'septieme': 7, 'sept': 7, 'huitieme': 8,
             'huit': 8, 'neuvieme': 9, 'neuf': 9, 'dixieme': 10, 'dix': 10, 'onzieme': 11, 'onze': 11, 'douxieme': 12,
             'douze': 12, 'treizieme': 13, 'treize': 13, 'quatorzieme': 14, 'quatorze': 14, 'quinzieme': 15,
             'quinze': 15, 'seizieme': 16, 'seize': 16, 'dix septieme': 17, 'dix-sept': 17, 'dix huitieme': 18,
             'dix huit': 18, 'dix neuvieme': 19, 'dix neuf': 19, 'vingtieme': 20, 'vingt': 20}
    if string in trans.keys():
        return trans[string]
    else:
        return None

# pricing_label_emmaus
**Projet de 3A CentraleSupélec option OSY - Latitudes x Label Emmaüs** 

CHEVALIER Arnaud, DIDELOT Gabrielle, MARTIN Pauline

Ce projet a été réalisé dans le cadre d'un projet de 3ème année à l'Ecole CentraleSupélec.
L'objectif est de permettre à Label Emmaüs de mieux référencer leur catalogue de livres afin d'en assurer un pricing fiable et cohérent.
Pour cela, l'enjeu est de regrouper dans un même cluster les livres considérés comme similaires, à savoir les livres de même titre et même auteur.


Un algorithme permet d'enregistrer les livres dans une base de donnée.  
A chaque nouvel enregistrement d'un livre, l'algorithme identifie son cluster de livres comparables et lui affecte ce numéro de cluster.

Le service est disponible sous forme d'une API, qui peut être requêtée de plusieurs manières :
* **/form :** renvoie un formulaire qui permet de renseigner les informations d'un livre
* **/results_search :** renvoie les livres comparables à celui renseigné dans le formulaire. A noter : le premier livre renvoyé correspond au livre requêté. Ainsi, si seul ce livre est retourné, alors aucun livre similaire n'a été détecté.
* **/query_and_update :** renvoie sous format json les livres similaires au livre renseigné en paramètre de la requête. De plus, ce livre est enregistré dans la base de données.
* **/query :** idem que query_and_update, mais le livre n'est pas enregistré en base de données.
* **/query_cluster :** renvoie sous format json les livres appartenant au cluster dont le numéro est renseigné en paramètre de la requête.








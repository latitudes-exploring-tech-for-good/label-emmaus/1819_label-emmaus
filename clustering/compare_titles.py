from fuzzywuzzy import fuzz


def token_set_title(ref, title):
    """
    Function which gives similarity between two titles
    :param ref: reference title
    :param title: title to compare
    :return: similarity int between 0 and 100
    """
    match = fuzz.token_set_ratio(ref, title)
    return match


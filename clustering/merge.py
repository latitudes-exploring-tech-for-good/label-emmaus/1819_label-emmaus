import pandas as pd
from sqlalchemy import Table, MetaData
from sqlalchemy.orm import sessionmaker
from classification.multi_tables_functions import find_clusters_from_authors, find_row_ids_from_clusters, \
    find_nearest_cluster


## Deprecated
# Those functions are specific to the intent of merging the splitted clustered database

def merge_tables(tables, cluster_dicts, author_dicts, con, dummy_names, threshold, merged_table_name):
    """
    Function that merges all the tables in one big table with cluster_ids
    :param tables: list of table names
    :param cluster_dicts: list of dicts of cluster => row_ids
    :param author_dicts: list of dicts of author => cluster_id
    :param con: sqlalchemy database engine
    :param dummy_names: list of common names
    :param threshold: compatibility thereshold
    :param merged_table_name: name of the final merged table
    :return: dict(author => id_cluster) and dict(id_cluser => row_id) corresponding to the final merged table
    """
    author_dict_merged = {}
    cluster_dict_merge = {}
    for i in range(len(tables)):
        print('cherche dans la table', tables[i])
        current_table = pd.read_sql_query('SELECT * FROM {}'.format(tables[i]), con=con, index_col=['index'])
        current_table['title_length'] = current_table.title.str.len()
        list_clusters = current_table.id_cluster.unique()
        print('longueur table', len(current_table))
        print('nb clusters', len(list_clusters))
        for id_cluster in list_clusters:
            # print('recherche semblable au cluster ', id_cluster)
            cluster = current_table[current_table['id_cluster'] == id_cluster]
            idx = cluster.title_length.idxmin()

            book = cluster.loc[[(idx)]]

            cluster_books, new_authors, new_clusters = find_clusters_in_other_tables(book, dummy_names, threshold, con,
                                                                                     tables[i + 1:],
                                                                                     author_dicts[i + 1:],
                                                                                     cluster_dicts[i + 1:])

            all_cluster = pd.concat([cluster, cluster_books])

            new_id_cluster = insert_in_merged_table(all_cluster, merged_table_name, con)
            author_dict_merged, cluster_dict_merge = update_dicts(author_dict_merged, cluster_dict_merge, all_cluster,
                                                                  new_id_cluster)

            current_table = current_table[current_table['id_cluster'] != id_cluster]

            author_dicts = author_dicts[:i + 1] + new_authors
            cluster_dicts = cluster_dicts[:i + 1] + new_clusters
    return author_dict_merged, cluster_dict_merge


def insert_in_merged_table(all_cluster, merged_table_name, con):
    """
    Function that insert the newly found cluster in the main table
    :param all_cluster: dataframe , newly found cluster
    :param merged_table_name: merged table name
    :param con: sqlalchemy database engine
    :return: id of the new inserted cluster
    """
    new_id_cluster = 1
    if merged_table_name in con.table_names():
        max_id_cluster = pd.read_sql_query('SELECT MAX(id_cluster) FROM {}'.format(merged_table_name), con=con).iloc[
            0, 0]
        if max_id_cluster:
            new_id_cluster = max_id_cluster + 1

    all_cluster['id_cluster'] = new_id_cluster
    all_cluster.to_sql(merged_table_name, con=con, if_exists='append')
    return new_id_cluster


def update_dicts(authors, clusters, all_cluster, new_id_cluster):
    """
    Function that updated the author and cluster dicts with the content of the newly created cluster
    :param authors: dict(author => id_cluster)
    :param clusters: dict(id_cluser => row_id)
    :param all_cluster: dataframe , newly found cluster
    :param new_id_cluster: id of the new cluster (the all_cluster input of the function)
    :return: updated dict(author => id_cluster) and updated dict(id_cluser => row_id)
    """
    clusters[new_id_cluster] = []
    for idx, row in all_cluster.iterrows():
        for author in row.author.split():
            if author in authors.keys():
                authors[author].append(row.id_cluster)
            else:
                authors[author] = [row.id_cluster]

        clusters[new_id_cluster].append(idx)
    return authors, clusters


def find_clusters_in_other_tables(book, dummy_names, threshold, con, tables, authors, clusters):
    """
    Function that performs the cluster search in several tables.
    :param book: & row dataframe with book information
    :param dummy_names: list of common author names
    :param threshold: compatibility threshold
    :param con: sqlalchemy database engine
    :param tables: list of tables names (the tables in which the search is performed)
    :param authors: list of dict(author => id_cluster) for each tables to look in
    :param clusters: list of dict(id_cluser => row_id) for each tables to look in
    :return: response = the newly formed cluster, new_authors = updated dict(author => id_cluster), new_clusters = updated dict(author => id_cluster)
    """
    response = pd.DataFrame(columns=book.columns)
    new_authors = list(authors)
    new_clusters = list(clusters)

    for i in range(len(authors)):
        candidates = find_clusters_from_authors(book, new_authors[i], dummy_names)
        if len(candidates):
            row_ids = find_row_ids_from_clusters(new_clusters[i], candidates)
            table_name = tables[i]
            candidates = []
            for row in row_ids:
                candidates.append(
                    pd.read_sql_query(
                        'SELECT * FROM {} WHERE "index" IN ('.format(table_name) + ','.join(map(str, row)) + ')',
                        con=con, index_col=['index']))

            max_score, output = find_nearest_cluster(candidates, book, threshold)

            if max_score[0] > 0:
                # il a trouve un cluster
                # print("j'ai trouvé ********************************************", tables[i])
                # print(book.values)
                # print(output.values)
                response = pd.concat([response, output])

                drop_cluster_rows(max_score, table_name, con)

                new_authors[i], new_clusters[i] = update_dicts(new_authors[i], new_clusters[i], output)

    return response, new_authors, new_clusters


def drop_cluster_rows(max_score, table_name, con):
    """
    Function that drops rows of a cluster in the table
    :param max_score: list[id_cluster, score]
    :param table_name: name of the table which rows will be dropped
    :param con: sqlalchemy database engine
    :return: number of deleted rows
    """
    # print('je drop le cluster ', max_score)
    metadata = MetaData()
    Session = sessionmaker(autoflush=True, autocommit=True)
    Session.configure(bind=con)
    session = Session()
    metadata.reflect(bind=con)
    table = Table(table_name, metadata, autoload_with=con)
    # print("table name", table_name)
    # print(type(max_score[0]))
    q = session.query(table).filter(table.c.id_cluster == int(max_score[0]))
    # print(q.all())
    n_deleted = q.delete(synchronize_session=False)
    # print("je supprime tant de lignes", n_deleted)
    return n_deleted

import pandas as pd
from clustering.compare_titles import token_set_title
import datetime as dt
import numpy as np

# All those functions were intents to apply the clustering over the whole database. However they contain the logics used
#  in the production version of the algorithm and it was the basis to buil the algo


def clustering_v_1(input_df, threshold):
    """
    :param input_df: dataframe to cluster [id(Index), author, title]
    :param threshold: threshold comparison title below which a new cluster is created
    :return output_data: dataframe to cluster [id(Index), author, title, id_cluster]
    :return cluster_dict: key = id_cluster, value = id
    :return author_dict: key = name, value = id
    """
    # initializing outputs
    cluster_len = 1
    author_dict = {}
    cluster_dict = {}

    access_authors = []
    levenstein = []

    # main loop : iterate over all the data frame to cluster
    for row in input_df.itertuples():
        # find candidates by authors
        candidates = []  # liste des clusters candidats
        max_score = [-1, 0]
        authors = str(row.author).split()
        starta = dt.datetime.now()
        for author in authors:
            if author in author_dict.keys():
                candidates.extend(author_dict[author])
            else:
                author_dict[author] = []

        candidates = set(candidates)
        stopa = dt.datetime.now()
        access_authors.append(stopa - starta)

        # refine candidates by title
        print("Ligne {} found {} clusters candidates".format(row.author, len(candidates)))

        startl = dt.datetime.now()
        if len(candidates) > 0:
            for candidate in candidates:
                row_ids = cluster_dict[candidate]
                titles = input_df[input_df.index.isin(set(row_ids))].title
                somme = 0
                for title in titles:
                    somme += token_set_title(title, row.title)
                avg = somme / len(titles)
                if avg >= threshold and avg > max_score[1]:
                    max_score = [candidate, avg]
        stopl = dt.datetime.now()
        levenstein.append(stopl - startl)

        # if a matching cluster is found add this book to the cluster
        if not max_score[0] == -1:
            cluster_dict[max_score[0]].append(row.Index)
            matching_cluster = max_score[0]

        # else create a new cluster
        else:
            cluster_dict[cluster_len] = [row.Index]
            matching_cluster = cluster_len
            cluster_len += 1

        # update authors dict
        for author in authors:
            if matching_cluster not in author_dict[author]:
                author_dict[author].append(matching_cluster)

    # append cluster column to output data
    output_df = input_df.copy()
    output_df['id_cluster'] = 0
    for key, values in cluster_dict.items():
        output_df.loc[values, 'id_cluster'] = key

    return author_dict, cluster_dict, output_df, access_authors, levenstein


def clustering_v_2(input_df, threshold, dummy_names):
    """
    :param input_df: dataframe to cluster [id(Index), author, title]
    :param threshold: threshold comparison title below which a new cluster is created
    :param dummy_names: authors names anad their frequency of occurence
    :return output_data: dataframe to cluster [id(Index), author, title, id_cluster]
    :return cluster_dict: key = id_cluster, value = id
    :return author_dict: key = name, value = id
    """
    # initializing outputs
    cluster_len = 1
    author_dict = {}
    cluster_dict = {}

    access_authors = []
    levenstein = []

    # main loop : iterate over all the data frame to cluster
    for row in input_df.itertuples():
        # find candidates by authors
        candidates = []  # liste des clusters candidats
        max_score = [-1, 0]
        authors = str(row.author).split()

        starta = dt.datetime.now()
        authors_copy = list(authors)
        # remove too frequent names
        for name in dummy_names:
            if len(authors_copy) <= 1:
                break
            else:
                if name in authors_copy:
                    authors_copy.remove(name)

        for author in authors_copy:
            if author in author_dict.keys():
                candidates.extend(author_dict[author])
            else:
                author_dict[author] = []

        candidates = set(candidates)
        stopa = dt.datetime.now()
        access_authors.append(stopa - starta)

        # refine candidates by title
        # print("Ligne {} found {} clusters candidates".format(row.author, len(candidates)))

        startl = dt.datetime.now()
        if len(candidates) > 0:
            for candidate in candidates:
                row_ids = cluster_dict[candidate]
                titles = input_df[input_df.index.isin(set(row_ids))].title
                somme = 0
                for title in titles:
                    somme += token_set_title(title, row.title)
                avg = somme / len(titles)
                if avg >= threshold and avg > max_score[1]:
                    max_score = [candidate, avg]
        stopl = dt.datetime.now()
        levenstein.append(stopl - startl)

        # if a matching cluster is found add this book to the cluster
        if not max_score[0] == -1:
            cluster_dict[max_score[0]].append(row.Index)
            matching_cluster = max_score[0]

        # else create a new cluster
        else:
            cluster_dict[cluster_len] = [row.Index]
            matching_cluster = cluster_len
            cluster_len += 1

        # update authors dict
        for author in authors:
            if author not in author_dict.keys():
                author_dict[author] = []
            if matching_cluster not in author_dict[author]:
                author_dict[author].append(matching_cluster)

    # append cluster column to output data
    output_df = input_df.copy()
    output_df['id_cluster'] = 0
    for key, values in cluster_dict.items():
        output_df.loc[values, 'id_cluster'] = key

    return author_dict, cluster_dict, output_df, access_authors, levenstein


def clustering_v_3(input_df, threshold, dummy_names):
    """
    :param input_df: dataframe to cluster [id(Index), author, title, VOLUME]
    :param threshold: threshold comparison title below which a new cluster is created
    :param dummy_names: authors names anad their frequency of occurence
    :return output_data: dataframe to cluster [id(Index), author, title, id_cluster]
    :return cluster_dict: key = id_cluster, value = id
    :return author_dict: key = name, value = id
    """
    # initializing outputs
    cluster_len = 1
    author_dict = {}
    cluster_dict = {}

    access_authors = []
    levenstein = []
    # main loop : iterate over all the data frame to cluster
    for row in input_df.itertuples():
        # find candidates by authors
        candidates = []  # liste des clusters candidats
        max_score = [-1, 0]
        authors = str(row.author).split()

        starta = dt.datetime.now()
        authors_copy = list(authors)
        # remove too frequent names
        for name in dummy_names:
            if len(authors_copy) <= 1:
                break
            else:
                if name in authors_copy:
                    authors_copy.remove(name)

        for author in authors_copy:
            if author in author_dict.keys():
                candidates.extend(author_dict[author])
            else:
                author_dict[author] = []

        candidates = set(candidates)
        stopa = dt.datetime.now()
        access_authors.append(stopa - starta)

        # refine candidates by title
        # print("Ligne {} found {} clusters candidates".format(row.author, len(candidates)))

        startl = dt.datetime.now()
        if len(candidates) > 0:
            for candidate in candidates:
                row_ids = cluster_dict[candidate]
                cands = input_df.loc[set(row_ids)]
                VOL = cands.iloc[0].VOLUME
                if row.VOLUME == VOL:
                    titles = cands.title
                    somme = 0
                    for title in titles:
                        somme += token_set_title(title, row.title)
                    avg = somme / len(titles)
                    if avg >= threshold and avg > max_score[1]:
                        max_score = [candidate, avg]
        stopl = dt.datetime.now()
        levenstein.append(stopl - startl)

        # if a matching cluster is found add this book to the cluster
        if not max_score[0] == -1:
            cluster_dict[max_score[0]].append(row.Index)
            matching_cluster = max_score[0]

        # else create a new cluster
        else:
            cluster_dict[cluster_len] = [row.Index]
            matching_cluster = cluster_len
            cluster_len += 1

        # update authors dict
        for author in authors:
            if author not in author_dict.keys():
                author_dict[author] = []
            if matching_cluster not in author_dict[author]:
                author_dict[author].append(matching_cluster)

    # append cluster column to output data
    output_df = input_df.copy()
    output_df['id_cluster'] = 0
    for key, values in cluster_dict.items():
        output_df.loc[values, 'id_cluster'] = key

    return author_dict, cluster_dict, output_df, access_authors, levenstein


def clustering_v_4(input_df, threshold, dummy_names):
    """
    :param input_df: dataframe to cluster [id(Index), author, title, VOLUME]
    :param threshold: threshold comparison title below which a new cluster is created
    :param dummy_names: authors names anad their frequency of occurence
    :return output_data: dataframe to cluster [id(Index), author, title, id_cluster]
    :return cluster_dict: key = id_cluster, value = id
    :return author_dict: key = name, value = id
    """
    # initializing outputs
    cluster_len = 1
    author_dict = {}
    cluster_dict = {}

    # main loop : iterate over all the data frame to cluster
    for row in input_df.itertuples():
        # find candidates by authors
        candidates = []  # liste des clusters candidats
        max_score = [-1, 0]
        authors = str(row.author).split()

        authors_copy = list(authors)
        # remove too frequent names
        for name in dummy_names:
            if len(authors_copy) <= 1:
                break
            else:
                if name in authors_copy:
                    authors_copy.remove(name)

        for author in authors_copy:
            if author in author_dict.keys():
                candidates.extend(author_dict[author])
            else:
                author_dict[author] = []

        candidates = set(candidates)

        # refine candidates by title
        # print("Ligne {} found {} clusters candidates".format(row.author, len(candidates)))

        if len(candidates) > 0:
            for candidate in candidates:
                row_ids = cluster_dict[candidate]
                cands = input_df.loc[set(row_ids)]
                VOL = cands.iloc[0].VOLUME
                if row.VOLUME == VOL:
                    titles = cands.title
                    somme = 0
                    for title in titles:
                        somme += token_set_title(title, row.title)
                    avg = somme / len(titles)
                    if avg >= threshold and avg > max_score[1]:
                        max_score = [candidate, avg]

        # if a matching cluster is found add this book to the cluster
        if not max_score[0] == -1:
            cluster_dict[max_score[0]].append(row.Index)
            matching_cluster = max_score[0]

        # else create a new cluster
        else:
            cluster_dict[cluster_len] = [row.Index]
            matching_cluster = cluster_len
            cluster_len += 1

        # update authors dict
        for author in authors:
            if author not in author_dict.keys():
                author_dict[author] = []
            if matching_cluster not in author_dict[author]:
                author_dict[author].append(matching_cluster)

    # append cluster column to output data
    output_df = input_df.copy()
    output_df['id_cluster'] = 0
    for key, values in cluster_dict.items():
        output_df.loc[values, 'id_cluster'] = key

    return author_dict, cluster_dict, output_df


def recursive_clustering(input_df, threshold):
    """
    Recursive implementation of the clustering
    :param input_df: dataframe to cluster [id(Index), author, title, VOLUME]
    :param threshold: threshold comparison title below which a new cluster is created
    :return: dataframe with cluster column
    """
    if len(input_df) == 1:
        input_df['id_cluster'] = 1
        return input_df
    else:
        input_G = input_df[:int(len(input_df) / 2)]
        input_D = input_df[int(len(input_df) / 2):]
        LG = recursive_clustering(input_G, threshold)
        LD = recursive_clustering(input_D, threshold)

        # fusion
        idx = 1
        output_df = pd.DataFrame()
        print("fusion : left ({}), right ({})".format(len(LG), len(LD)))
        while len(LG) or len(LD):
            if len(LG) and len(LD):
                ref_g = LG.iloc[0]
                extract_g = LG[LG['id_cluster'] == ref_g['id_cluster']]
                extract_g['id_cluster'] = idx
                output_df = pd.concat([output_df, extract_g])
                LG = LG[LG['id_cluster'] != ref_g['id_cluster']]

                search = True
                idx_d = 0
                while search:
                    ref_d = LD.iloc[idx_d]
                    comparable = compare(ref_g, ref_d, threshold)
                    if comparable:
                        extract_d = LD[LD['id_cluster'] == ref_d['id_cluster']]
                        extract_d['id_cluster'] = idx
                        output_df = pd.concat([output_df, extract_d])
                        LD = LD[LD['id_cluster'] != ref_d['id_cluster']]
                        search = False
                    idx_d += 1
                    if len(LD) == idx_d:
                        search = False

            elif len(LG) and not len(LD):
                ref_g = LG.iloc[0]
                extract_g = LG[LG['id_cluster'] == ref_g['id_cluster']]
                extract_g['id_cluster'] = idx
                output_df = pd.concat([output_df, extract_g])
                LG = LG[LG['id_cluster'] != ref_g['id_cluster']]

            elif not len(LG) and len(LD):
                ref_d = LD.iloc[0]
                extract_d = LD[LD['id_cluster'] == ref_d['id_cluster']]
                extract_d['id_cluster'] = idx
                output_df = pd.concat([output_df, extract_d])
                LD = LD[LD['id_cluster'] != ref_d['id_cluster']]
            idx += 1
        return output_df


def compare(ref_g, ref_d, threshold):
    """
    Function that compares the two references and returns true or false if they are similar enough
    :param ref_g: first reference
    :param ref_d: second reference
    :param threshold: minimum compatibility to be comparable
    :return: boolean
    """
    author_ratio = token_set_title(ref_g['author'], ref_d['author'])
    title_ratio = token_set_title(ref_g['title'], ref_d['title'])
    if author_ratio * title_ratio / 100 >= threshold:
        return True
    else:
        return False


def frequent_authors(data, seuil):
    """
    Function that builds the ordered common names list from the data
    :param data: dataframe with "author" column
    :param seuil: minimum percentage of apparitions in rows above which a name is considered common
    :return: list of common names
    """
    # extract author column
    authors = data.author
    authors.dropna(inplace=True)
    # list of all names contained in author
    names = []
    for auth in authors:
        auth = auth.split()
        names.extend(auth)
    # count frequency of e ach name
    freq = {}
    for name in names:
        if name not in freq.keys():
            freq[name] = 1
        else:
            freq[name] += 1
    # Select those above seuil and sort descending
    maxi = len(authors)
    dummy_names = []
    dummy_freq = []
    for key, value in freq.items():
        if value / maxi >= seuil / 100:
            dummy_names.append(key)
            dummy_freq.append(value / maxi)
    ordered = np.argsort(dummy_freq)[::-1]
    dummy_names = [dummy_names[idx] for idx in ordered]
    return dummy_names


def frequent_titles(data, seuil):
    """
    Function that builds the ordered common title words list from the data
    :param data: dataframe with "title" column
    :param seuil: minimum percentage of apparitions in rows above which a word is considered common
    :return: list of common words
    """
    # extract author column
    authors = data.title
    authors.dropna(inplace=True)
    # list of all names contained in author
    names = []
    for auth in authors:
        auth = auth.split()
        names.extend(auth)
    # count frequency of e ach name
    freq = {}
    for name in names:
        if name not in freq.keys():
            freq[name] = 1
        else:
            freq[name] += 1
    # Select those above seuil and sort descending
    maxi = len(authors)
    dummy_names = []
    dummy_freq = []
    for key, value in freq.items():
        if value / maxi >= seuil / 100:
            dummy_names.append(key)
            dummy_freq.append(value / maxi)
    ordered = np.argsort(dummy_freq)[::-1]
    dummy_names = [dummy_names[idx] for idx in ordered]
    return dummy_names

# Fonctions utilisées pour la version multitables de la classification.
# Pas utilisées dans la version finale

import pandas as pd
from classification.api_functions import find_nearest_cluster
import pre_processing.cleaning
import pre_processing.volume_number_detection
import json


def find_clusters_from_authors(book, author_dict, dummy_names):
    """
    Return the ids of the clusters corresponding to the author
    :param book: a dataframe with one row
    :param author_dict: a dictionnary of author => cluster_id
    :param dummy_names: list of common names
    :return: set of clusters_ids
    """
    authors_copy = book.iloc[0]['author'].split()
    for name in dummy_names:
        if len(authors_copy) <= 1:
            break
        else:
            if name in authors_copy:
                authors_copy.remove(name)
    clusters = []
    for name in authors_copy:
        if name in author_dict.keys():
            clusters.extend(author_dict[name])
    return set(clusters)


def find_row_ids_from_clusters(cluster_dict, clusters):
    """
    Return the row_ids for the books corresponding to each cluster
    :param cluster_dict: dict cluster_id => row_ids
    :param clusters: list of clusters ids
    :return: list of lists of row_ids
    """
    row_ids = []
    for cluster in clusters:
        row_ids.append(cluster_dict[cluster])
    return row_ids


def get_response(book, dummy_names, threshold, l, con, authors, clusters):
    """
    Return all the books belonging to the cluster of the querried book
    :param book: 1 row dataframe
    :param dummy_names: list of common names
    :param threshold: compatibility threshold
    :param l: length of the tables
    :param con: sqlalchemy database engine
    :param authors: list of dicts author => id cluster
    :param clusters: list of dicts id cluster => row id
    :return: dataframe
    """
    response = pd.DataFrame()

    for i in range(len(authors)):
        candidates = find_clusters_from_authors(book, authors[i], dummy_names)
        print('found', len(candidates), 'clusters candidates in slice', l * i, 'to', l * (i + 1) - 1)
        if len(candidates):
            row_ids = find_row_ids_from_clusters(clusters[i], candidates)

            table_name = 'clustered_no_sort_from_' + str(i * l) + '_to_' + str((i + 1) * l - 1)
            candidates = []
            for row in row_ids:
                candidates.append(
                    pd.read_sql_query(
                        'SELECT * FROM {} WHERE "index" IN ('.format(table_name) + ','.join(map(str, row)) + ')',
                        con=con, index_col=['index']))
            # file_name = path + '/data/clustered/clustered_df_from_' + str(i * l) + '_to_' + str((i + 1) * l - 1)
            # df = pd.read_csv(file_name, index_col=['Unnamed: 0'])
            max_score, output = find_nearest_cluster(candidates, book, threshold)
            if max_score[0] > 0:
                # il a trouve un cluster
                response = pd.concat([response, output])
    return response


def get_similar_books(data, engine, common_names, l, authors, clusters):
    """
    Fuction called by the route, pre-processes the request body content and formates the output
    :param data: 1 row dataframe with queried book information
    :param engine: sqlalchemy database engine
    :param common_names: list of common names
    :param l: length of the tables
    :param authors: list of dicts author => id cluster
    :param clusters: list of dicts id cluster => row id
    :return: dataframe
    """
    # preprocessing
    data['author'] = pre_processing.cleaning.clean_df_column(data.AUTEUR)
    data['title'] = pre_processing.cleaning.clean_df_column(data.TITRE)
    data = pre_processing.volume_number_detection.add_volume_column_and_replace_title(data)

    # get response
    threshold = 85

    resp = get_response(data, common_names, threshold, l, engine, authors, clusters)
    if len(resp) > 0:
        resp = resp[['ASIN', 'ISBN', 'AUTEUR', 'TITRE', 'PUBLICATION_DATE', 'PUBLISHER', 'VOLUME']]
        return json.loads(resp.to_json(orient='records'))
    else:
        return []

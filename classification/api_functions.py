import pandas as pd
import pre_processing.cleaning
import pre_processing.volume_number_detection
from clustering.compare_titles import token_set_title


def find_cluster_and_insert(book, con, frequent_names, insert, threshold=85):
    """
    functioncalled by the api to find cluster of a book (and insert the book to the database)
    :param book: 1 row dataframe with book information
    :param con: sqlalchemy database engine
    :param frequent_names: list od frequent names
    :param insert: boolean inset the book in the db or not
    :param threshold: minimum compatibility to say that the book belongs to the cluster
    :return: dataframe with all books in the cluster. The first row is the book itself.
    """
    # preprocessing
    book['author'] = pre_processing.cleaning.clean_df_column(book.AUTEUR)
    book['title'] = pre_processing.cleaning.clean_df_column(book.TITRE)
    book = pre_processing.volume_number_detection.add_volume_column_and_replace_title(book)

    # response

    clusters = find_clusters_from_authors_table(book, frequent_names, con=con)
    print('found', len(clusters), 'clusters candidates')

    output = pd.DataFrame()
    max_score = [-1, 0]

    if len(clusters):
        # row_ids = find_row_ids_from_clusters_table(clusters, con=con)

        candidates = []
        # for row in row_ids:
        #     candidates.append(
        #         pd.read_sql_query(
        #             'SELECT * FROM main_clustered_table WHERE "index" IN (' + ','.join(map(str, row)) + ')',
        #             con=con, index_col=['index']))
        for cluster in clusters:
            candidates.append(
                pd.read_sql_query(
                    """SELECT * FROM main_clustered_table WHERE id_cluster={}""".format(int(cluster)),
                    con=con, index_col=['index']))
        max_score, output = find_nearest_cluster(candidates, book, threshold)
    if max_score[0] == -1:
        max_id_cluster = pd.read_sql_query('SELECT MAX(id_cluster) FROM main_clustered_table', con=con).iloc[
            0, 0]
        book['id_cluster'] = max_id_cluster + 1
        book['score'] = 100
    else:
        book['id_cluster'] = max_score[0]
        book['score'] = max_score[1]

    row_id = pd.read_sql_query('SELECT count(*) FROM main_clustered_table', con=con).iloc[0, 0]
    book.index = [row_id + 1]
    response = pd.concat([book, output])

    if insert:
        # insert in database
        update_tables(book, con)

    return response


def find_clusters_from_authors_table(book, frequent_names, con):
    """
    Function queries authors database to find pertinent clusters. It will not query common names
    :param book: 1 row dataframe with book information
    :param frequent_names: list od frequent names
    :param con: sqlalchemy database engine
    :return: set of cluster numbers
    """
    authors_copy = book.iloc[0]['author'].split()
    for name in frequent_names:
        if len(authors_copy) <= 1:
            break
        else:
            if name in authors_copy:
                authors_copy.remove(name)
    clusters = []
    for name in authors_copy:
        clusters.extend(
            pd.read_sql_query("""SELECT cluster FROM author_cluster WHERE "index"='{}'""".format(name),
                              con=con).T.values[0])
    return set(clusters)


## Deprecated
def find_row_ids_from_clusters_table(clusters, con):
    """
    function that looks for row ids in a clusters table
    :param clusters: list of cluster numbers
    :param con: sqlqlchemy database engine
    :return: list of list of row ids for each cluster number
    """
    row_ids = []
    for cluster in clusters:
        row_ids.append(
            pd.read_sql_query("""SELECT row FROM cluster_row WHERE "index"='{}'""".format(cluster), con=con).T.values[
                0])
    return row_ids


def update_tables(book, con):
    """
    function that updates the database when a book is added to the main_clustered_table
    :param book: 1 row dataframe with book information
    :param con: sqlalchemy database engine
    :return:
    """
    authors_copy = book.author.values[0].split()
    id_cluster = book.id_cluster.values[0]
    for name in authors_copy:
        clusters = set(pd.read_sql_query("""SELECT cluster FROM author_cluster WHERE "index"='{}'""".format(name),
                                         con=con).T.values[0])
        if id_cluster not in clusters:
            auth_clust = pd.DataFrame(data=[[id_cluster]], columns=['cluster'], index=[[name]])
            auth_clust.to_sql('author_cluster', con=con, if_exists='append')

    # clust_row = pd.DataFrame(data=[[book.index.values[0]]], columns=['row'], index=[[id_cluster]])
    # clust_row.to_sql('cluster_row', con=con, if_exists='append')

    book.to_sql('main_clustered_table', con=con, if_exists='append')


def find_nearest_cluster(candidates, book, threshold):
    """
    function that given a list of cluster rows dataframes finds the best match with the book
    :param candidates: list of clusters dataframes
    :param book: & row dataframe with book information
    :param threshold: minimum compatibility to say that the book belongs to the cluster
    :return: max_score = [id_cluster, compatibility], output = dataframe of books in the matching cluster
    """
    max_score = [-1, 0]
    output = pd.DataFrame()
    for candidate in candidates:
        candidate = candidate.where((pd.notnull(candidate)), None)
        # volume selection
        VOL = candidate.iloc[0]['VOLUME']
        if VOL is not None and book.iloc[0]['VOLUME'] is not None:
            if int(book.iloc[0]['VOLUME']) == int(VOL):
                clust = candidate.iloc[0]['id_cluster']
                titles = candidate.title
                somme = 0
                for title in titles:
                    somme += token_set_title(title, book.iloc[0]['title'])
                avg = somme / len(titles)
                if avg >= threshold and avg > max_score[1]:
                    max_score = [clust, avg]
                    output = candidate
        else:
            raise TypeError('VOLUME must be of type int, got None')
    return max_score, output


def get_cluster_by_id(cluster, con):
    """
    function triggered when the api root add query_cluster if fired
    :param cluster: the querried cluster number
    :param con: sqlalchemy database engine
    :return: dataframe with all books in the cluster
    """
    response = pd.read_sql_query("""SELECT * FROM main_clustered_table WHERE id_cluster={}""".format(int(cluster)),
                                 con=con, index_col=['index'])
    return response

import pandas as pd
import os
import json
import pickle
from flask import Flask, jsonify, render_template, request
from sqlalchemy import create_engine
from classification.api_functions import find_cluster_and_insert, get_cluster_by_id

here = os.path.dirname(__file__)


app = Flask(__name__)
engine = create_engine('sqlite:///clusters.db', echo=False)
# common names
common_names = pickle.load(open(os.path.join(here, 'data/common_names'), 'rb'))


@app.route('/form', methods=['GET', 'POST'])
def form():
    return render_template('form.html', title='Formulaire')


@app.route('/results_search', methods=['POST'])
def results_search():
    title = request.form['title']
    author = request.form['author']
    isbn = request.form['isbn']
    edition = request.form['edition']
    date = request.form['date']
    book = pd.DataFrame(
        {'ISBN': isbn, 'ASIN': '', 'TITRE': title, 'AUTEUR': author, 'PUBLISHER': edition, 'PUBLICATION_DATE': date},
        index=[0])
    results = find_cluster_and_insert(book=book, con=engine, frequent_names=common_names,
                                                         insert=False, threshold=85)
    results = json.loads(results.to_json(orient='records'))
    return render_template('results_search.html', query=title, resultats=results)


@app.route('/api/query_and_update', methods=['POST'])
def api_results_query_update():
    request_body = request.get_json()
    print(request_body)

    if 'author' not in request_body.keys() or not request_body['author']:
        return jsonify('Missing author'), 400
    if 'title' not in request_body.keys() or not request_body['title']:
        return jsonify('Missing title'), 400

    title = request_body['title']
    author = request_body['author']
    isbn, edition, date, asin = '', '', '', ''
    if 'isbn' in request_body.keys():
        isbn = request_body['isbn']
    if 'edition' in request_body.keys():
        edition = request_body['edition']
    if 'date' in request_body.keys():
        date = request_body['date']
    if 'asin' in request_body.keys():
        asin = request_body['asin']

    book = pd.DataFrame(
        {'ISBN': isbn, 'ASIN': asin, 'TITRE': title, 'AUTEUR': author, 'PUBLISHER': edition, 'PUBLICATION_DATE': date},
        index=[0])
    results = find_cluster_and_insert(book=book, con=engine, frequent_names=common_names,
                                                         insert=True, threshold=85)
    response = json.loads(results.to_json(orient='records'))
    return jsonify(response), 200


@app.route('/api/query', methods=['POST'])
def api_results_query():
    request_body = request.get_json()
    print(request_body)

    if 'author' not in request_body.keys() or not request_body['author']:
        return jsonify('Missing author'), 400
    if 'title' not in request_body.keys() or not request_body['title']:
        return jsonify('Missing title'), 400

    title = request_body['title']
    author = request_body['author']
    isbn, edition, date, asin = '', '', '', ''
    if 'isbn' in request_body.keys():
        isbn = request_body['isbn']
    if 'edition' in request_body.keys():
        edition = request_body['edition']
    if 'date' in request_body.keys():
        date = request_body['date']
    if 'asin' in request_body.keys():
        asin = request_body['asin']

    book = pd.DataFrame(
        {'ISBN': isbn, 'ASIN': asin, 'TITRE': title, 'AUTEUR': author, 'PUBLISHER': edition, 'PUBLICATION_DATE': date},
        index=[0])
    results = find_cluster_and_insert(book=book, con=engine, frequent_names=common_names,
                                                         insert=False, threshold=85)
    response = json.loads(results.to_json(orient='records'))
    return jsonify(response), 200


@app.route('/api/query_cluster', methods=['POST'])
def api_results_query_cluster():
    request_body = request.get_json()
    print(request_body)

    if 'id_cluster' not in request_body.keys():
        return jsonify('Missing id_cluster'), 400

    results = get_cluster_by_id(cluster=request_body['id_cluster'], con=engine)
    response = json.loads(results.to_json(orient='records'))
    return jsonify(response), 200


if __name__ == '__main__':
    app.run()

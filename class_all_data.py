# Script to fill database with data we already have

import pandas as pd
from classification.api_functions import find_cluster_and_insert
from sqlalchemy import create_engine
from clustering import algorithms
import pickle
import pre_processing.cleaning

engine = create_engine('sqlite:///clusters.db', echo=False)

print("loading book ...")
data = pd.read_csv('data/amazon_livres.txt', sep="\t", dtype=str, encoding="ISO-8859-1")
data = data[['ASIN', 'ISBN', 'TITLE', 'AUTHOR', 'PUBLISHER', 'PUBLICATION_DATE']]
data = data[data['AUTHOR'].notnull()]
data = data[data['TITLE'].notnull()]
data.rename({'TITLE': 'TITRE', 'AUTHOR': 'AUTEUR'}, axis=1, inplace=True)

data['author'] = pre_processing.cleaning.clean_df_column(data.AUTEUR)
data['title'] = pre_processing.cleaning.clean_df_column(data.TITRE)
data = data[data['author'] != 'collectif']

print("common names ...")
common_names = algorithms.frequent_authors(data, 0.25)
pickle.dump(common_names, open('data/frequent_names', 'wb'))

data = data.iloc[100000:100020]  # books to insert

book = pd.DataFrame
book_id = 0

try:
    for book_id, book_item in data.iterrows():
        book = book_item.to_frame()
        book = book.T
        find_cluster_and_insert(book=book, con=engine, frequent_names=common_names,
                                insert=True, threshold=85)
finally:
    print("last_row", book_id)
    print("last_book", book)

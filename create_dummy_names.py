# Script creating a file with the most frequent words in the 'AUTHOR' field

import clustering.algorithms
import pre_processing.cleaning
import pre_processing.volume_number_detection
import pandas as pd
import pickle

print("loading dataset ...")
data = pd.read_csv("data/amazon_livres.txt", sep="\t", dtype=str, encoding="ISO-8859-1")
data = data[['ASIN', 'ISBN', 'TITLE', 'AUTHOR', 'PUBLISHER', 'PUBLICATION_DATE', 'PARUTION_ID']]
data = data[data['AUTHOR'].notnull()]
data = data[data['TITLE'].notnull()]
data.rename({'AUTHOR': 'author', 'TITLE': 'title'}, axis=1, inplace=True)

print("pre-processing ...")
data.author = pre_processing.cleaning.clean_df_column(data.author)
data.title = pre_processing.cleaning.clean_df_column(data.title)
data = pre_processing.volume_number_detection.add_volume_column_and_replace_title(data)

print("frequent names ...")
dummy_names = clustering.algorithms.frequent_authors(data, 0.25) # the threshold can be modified
pickle.dump(dummy_names, open('data/common_names', 'wb'))
print("clustering ...")
# Script to initialize database, by adding one book

import pandas as pd
import pre_processing.cleaning
import pre_processing.volume_number_detection
from sqlalchemy import create_engine


engine = create_engine('sqlite:///clusters.db', echo=False)

book = pd.DataFrame(
        {'ISBN': '2221075943', 'ASIN':'2221075943', 'TITRE': ' La tête dans le carton à chapeaux', 'AUTEUR': 'Mark Childress', 'PUBLISHER': 'Robert Laffont', 'PUBLICATION_DATE': '1995-02-28'}, index=[0])

book['author'] = pre_processing.cleaning.clean_df_column(book.AUTEUR)
book['title'] = pre_processing.cleaning.clean_df_column(book.TITRE)
book = pre_processing.volume_number_detection.add_volume_column_and_replace_title(book)

book['id_cluster'] = 1
book['score'] = 100

book.index = [1]

authors_copy = book.author.values[0].split()
id_cluster = book.id_cluster.values[0]

for name in authors_copy:
    auth_clust = pd.DataFrame(data=[[id_cluster]], columns=['cluster'], index=[[name]])
    auth_clust.to_sql('author_cluster', con=engine, if_exists='append')

clust_row = pd.DataFrame(data=[[book.index.values[0]]], columns=['row'], index=[[id_cluster]])
clust_row.to_sql('cluster_row', con=engine, if_exists='append')

book.to_sql('main_clustered_table', con=engine, if_exists='append')
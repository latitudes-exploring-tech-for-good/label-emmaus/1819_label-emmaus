# Script to perform the clustering by spliting the dataset into several subtables

import clustering.algorithms
import pre_processing.cleaning
import pre_processing.volume_number_detection
import pandas as pd
import pickle
from concurrent import futures
from sqlalchemy import create_engine

l = 25000

print("creating database engine ...")
engine = create_engine('sqlite:///clusters.db', echo=False)

print("loading dataset ...")
data = pd.read_csv("data/amazon_livres.txt", sep="\t", dtype=str, encoding="ISO-8859-1")
data = data[['ASIN', 'ISBN', 'TITLE', 'AUTHOR', 'PUBLISHER', 'PUBLICATION_DATE', 'PARUTION_ID']]
data = data[data['AUTHOR'].notnull()]
data = data[data['TITLE'].notnull()]
data.rename({'TITLE': 'TITRE', 'AUTHOR': 'AUTEUR'}, axis=1, inplace=True)

# data = data.iloc[0:5257]

print("pre-processing ...")
data['author'] = pre_processing.cleaning.clean_df_column(data.AUTEUR)
data.author = data.author.apply(lambda x: pre_processing.cleaning.sort_string(x))

data['title'] = pre_processing.cleaning.clean_df_column(data.TITRE)
data = pre_processing.volume_number_detection.add_volume_column_and_replace_title(data)

# print("sorting data along authors ...")
# data = data.sort_values(['author'])

print("dummy names ...")
dummy_names = clustering.algorithms.frequent_authors(data, 0.25)
pickle.dump(dummy_names, open('data/common_names', 'wb'))
print("clustering ...")


def cluster(start, stop, data, dummy_names):
    author_dict, cluster_dict, output_df = clustering.algorithms.clustering_v_4(data, 85, dummy_names)
    table_name = 'clustered_no_sort_from_' + str(start) + '_to_' + str(stop - 1)
    # output_df.to_sql(table_name, con=con, if_exists='replace')
    output_df.to_csv('data/clustered/clustered_nosort_from_' + str(start) + '_to_' + str(stop - 1))
    pickle.dump(cluster_dict, open('data/clusters/cluster_nosort_dict_from_' + str(start) + '_to_' + str(stop - 1), 'wb'))
    pickle.dump(author_dict, open('data/authors/author_nosort_dict_from_' + str(start) + '_to_' + str(stop - 1), 'wb'))
    return output_df, table_name


n = int(len(data) / l) + 1
start = []
stop = []
datas = []
dn = []
for i in range(n):
    deb = i * l
    fin = (i + 1) * l
    start.append(deb)
    stop.append(fin)
    datas.append(data.iloc[deb:min(fin, len(data))])
    dn.append(dummy_names)

st = pd.datetime.now()
with futures.ProcessPoolExecutor() as pool:
    for output_df, table_name in pool.map(cluster, start, stop, datas, dn):
        output_df.to_sql(table_name, con=engine, if_exists='replace')
        print(table_name)
stp = pd.datetime.now()

print('clustering time :', str(stp-st))

print("finished !")

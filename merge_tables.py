# Script which merges the subtables

from clustering import merge
from sqlalchemy import create_engine
import os
here = os.path.dirname(__file__)
import pickle


engine = create_engine('sqlite:///clusters.db', echo=False)
authors = []
clusters = []
tables = []
l = 25000
for i in range(len(os.listdir(os.path.join(here, 'data/authors/')))):
    start = i * l
    stop = (i + 1) * l - 1
    authors.append(
        pickle.load(open(os.path.join(here, 'data/authors/author_nosort_dict_from_' + str(start) + '_to_' + str(stop)), 'rb')))
    clusters.append(
        pickle.load(open(os.path.join(here, 'data/clusters/cluster_nosort_dict_from_' + str(start) + '_to_' + str(stop)), 'rb')))
    tables.append('clustered_no_sort_from_' + str(i * l) + '_to_' + str((i + 1) * l - 1))
# common names
common_names = pickle.load(open(os.path.join(here, 'data/common_names'), 'rb'))

table_name = "merged_table"

print(tables)
auth_dict, clust_dict = merge.merge_tables(tables, clusters, authors, engine, common_names, 85, table_name)

pickle.dump(auth_dict, open('merged_auth_dict.pickle', 'wb'))
pickle.dump(clust_dict, open('merged_clust_dict.pickle', 'wb'))
